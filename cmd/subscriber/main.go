package main

import (
	"context"
	"github.com/nats-io/stan.go"
	"gitlab.com/white-prince/wbtechl0/internal/consts"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"gitlab.com/white-prince/wbtechl0/internal/server"
	"gitlab.com/white-prince/wbtechl0/internal/transport/rest"
	"log"
)

func main() {
	clientID := "subscriber"
	srv := server.New(consts.Port)
	ctx := context.Background()

	db, err := models.InitDB()
	if err != nil {
		log.Fatalf("error occured while init db: %v", err)
	}

	handlers := rest.New(ctx, db)
	err = models.RestoreCacheFromDB(db, handlers.Cache)
	if err != nil {
		log.Fatalf("error occured while restore cache from db: %v", err)
	}
	log.Println(handlers.Cache)

	srv.InitHandler(handlers.InitRoutes())

	sc, err := stan.Connect(consts.ClusterID, clientID, stan.NatsURL(consts.NatsURL))
	if err != nil {
		log.Fatalf("Error connect to NATS Streaming: %v", err)
	}
	defer sc.Close()

	_, err = sc.Subscribe(consts.ChannelName, handlers.SubscribeHandler, stan.DurableName(consts.DurableName))
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Subscriber is running.")
	if err = srv.Run(); err != nil {
		log.Fatalf("error occured while running http server: %v", err)
	}
	select {}
}
