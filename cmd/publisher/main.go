package main

import (
	"encoding/json"
	"github.com/nats-io/stan.go"
	"gitlab.com/white-prince/wbtechl0/internal/consts"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"gitlab.com/white-prince/wbtechl0/pkg/generate"
	"log"
	"math/rand"
	"time"
)

func main() {
	clientID := "publisher"
	data := generate.New()
	rand.Seed(time.Now().UnixNano())

	sc, err := stan.Connect(consts.ClusterID, clientID, stan.NatsURL(consts.NatsURL))
	if err != nil {
		log.Fatal(err)
	}
	defer sc.Close()

	for {
		order := models.Order{
			OrderUID:          generate.RandomString(16),
			TrackNumber:       "WBILMTESTTRACK",
			Entry:             "WBIL",
			Delivery:          generate.RandomDelivery(data),
			Payment:           generate.RandomPayment(),
			Items:             []models.Item{generate.RandomItem()},
			Locale:            "en",
			InternalSignature: "",
			CustomerID:        rand.Intn(100),
			DeliveryService:   "meest",
			ShardKey:          "9",
			SmID:              rand.Intn(100),
			DateCreated:       time.Now().Format(time.RFC3339),
			OofShard:          "1",
		}
		
		orderBytes, err := json.Marshal(order)
		if err != nil {
			log.Fatalln(err)
		}

		if err = sc.Publish("orders", orderBytes); err != nil {
			log.Fatal(err)
		}
		formattedJSON, err := json.MarshalIndent(order, "", "    ")
		if err != nil {
			log.Fatalf("Error MarshalIndent formatted JSON: %v", err)
		}
		log.Printf("Message send: %s", string(formattedJSON))
		time.Sleep(1 * time.Second)
	}

}
