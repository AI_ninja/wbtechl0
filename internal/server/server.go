package server

import (
	"net/http"
	"time"
)

type Server struct {
	HttpServer *http.Server
}

func New(port string) *Server {
	return &Server{
		HttpServer: &http.Server{
			Addr:           ":" + port,
			ReadTimeout:    10 * time.Second,
			WriteTimeout:   10 * time.Second,
			MaxHeaderBytes: 1 << 20,
		},
	}
}

func (s *Server) InitHandler(handler http.Handler) {
	s.HttpServer.Handler = handler
}

func (s *Server) Run() error {
	return s.HttpServer.ListenAndServe()
}
