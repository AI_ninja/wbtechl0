package consts

const (
	ClusterID   = "wb-cluster"
	NatsURL     = "nats://localhost:4222"
	ChannelName = "orders"
	Port        = "8080"
	DurableName = "wb-orders"
)
