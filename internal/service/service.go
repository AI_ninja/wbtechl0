package service

import (
	"errors"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"sync"
)

func WriteToCache(mu *sync.Mutex, cache map[string]models.Order, key string, value models.Order) {
	mu.Lock()
	cache[key] = value
	mu.Unlock()
}
func MessageValidation(order *models.Order) error {
	if order.OrderUID == "" {
		return errors.New("empty UID")
	} else if order.Delivery.Name == "" {
		return errors.New("empty delivery name")
	} else if len(order.Items) == 0 {
		return errors.New("items is empty")
	}

	for _, item := range order.Items {
		if item.TrackNumber == "" {
			return errors.New("empty item track number")
		}
	}
	return nil
}
