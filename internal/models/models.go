package models

import (
	"fmt"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"time"
)

type Database interface {
	Create(value interface{}) *gorm.DB
}

type Order struct {
	OrderUID    string `formatted:"order_uid" gorm:"column:order_uid;primaryKey"`
	TrackNumber string `formatted:"track_number" gorm:"column:track_number"`
	Entry       string `formatted:"entry" gorm:"column:entry"`

	Delivery Delivery `formatted:"delivery" gorm:"-"`
	Payment  Payment  `formatted:"payment" gorm:"-"`
	Items    []Item   `formatted:"items" gorm:"-"`

	Locale            string `formatted:"locale" gorm:"column:locale"`
	InternalSignature string `formatted:"internal_signature" gorm:"column:internal_signature"`
	CustomerID        int    `formatted:"customer_id" gorm:"column:customer_id"`
	DeliveryService   string `formatted:"delivery_service" gorm:"column:delivery_service"`
	ShardKey          string `formatted:"shardkey" gorm:"column:shardkey"`
	SmID              int    `formatted:"sm_id" gorm:"column:sm_id"`
	DateCreated       string `formatted:"date_created" gorm:"column:date_created"`
	OofShard          string `formatted:"oof_shard" gorm:"column:oof_shard"`
}

type Delivery struct {
	OrderUID string `formatted:"-" gorm:"column:order_uid;foreignKey:OrderUID"`

	Name    string `formatted:"name" gorm:"column:name"`
	Phone   string `formatted:"phone" gorm:"column:phone"`
	Zip     string `formatted:"zip" gorm:"column:zip"`
	City    string `formatted:"city" gorm:"column:city"`
	Address string `formatted:"address" gorm:"column:address"`
	Region  string `formatted:"region" gorm:"column:region"`
	Email   string `formatted:"email" gorm:"column:email"`
}

type Payment struct {
	OrderUID string `formatted:"-" gorm:"column:order_uid;foreignKey:OrderUID"`

	Transaction  string    `formatted:"transaction" gorm:"column:transaction"`
	RequestID    string    `formatted:"request_id" gorm:"column:request_id"`
	Currency     string    `formatted:"currency" gorm:"column:currency"`
	Provider     string    `formatted:"provider" gorm:"column:provider"`
	Amount       int       `formatted:"amount" gorm:"column:amount"`
	PaymentDt    time.Time `formatted:"payment_dt" gorm:"column:payment_dt"`
	Bank         string    `formatted:"bank" gorm:"column:bank"`
	DeliveryCost int       `formatted:"delivery_cost" gorm:"column:delivery_cost"`
	GoodsTotal   int       `formatted:"goods_total" gorm:"column:goods_total"`
	CustomFee    int       `formatted:"custom_fee" gorm:"column:custom_fee"`
}

type Item struct {
	OrderUID string `formatted:"-" gorm:"column:order_uid;foreignKey:OrderUID"`

	ChrtID      int    `formatted:"chrt_id" gorm:"column:chrt_id"`
	TrackNumber string `formatted:"track_number" gorm:"column:track_number"`
	Price       int    `formatted:"price" gorm:"column:price"`
	Rid         string `formatted:"rid" gorm:"column:rid"`
	Name        string `formatted:"name" gorm:"column:name"`
	Sale        int    `formatted:"sale" gorm:"column:sale"`
	Size        string `formatted:"size" gorm:"column:size"`
	TotalPrice  int    `formatted:"total_price" gorm:"column:total_price"`
	NmID        int    `formatted:"nm_id" gorm:"column:nm_id"`
	Brand       string `formatted:"brand" gorm:"column:brand"`
	Status      int    `formatted:"status" gorm:"column:status"`
}

func InitDB() (*gorm.DB, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, err
	}

	dbHost := os.Getenv("DB_HOST")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbSSLMode := os.Getenv("DB_SSLMODE")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=%s", dbHost, dbUser, dbPassword, dbName, dbSSLMode)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func RestoreCacheFromDB(db *gorm.DB, cache map[string]Order) error {
	var orders []Order
	result := db.Find(&orders)
	if result.Error != nil {
		return result.Error
	}

	for _, order := range orders {
		var delivery Delivery
		if err := db.Where("order_uid = ?", order.OrderUID).First(&delivery).Error; err != nil {
			return err
		}
		order.Delivery = delivery

		var payment Payment
		if err := db.Where("order_uid = ?", order.OrderUID).First(&payment).Error; err != nil {
			return err
		}
		order.Payment = payment

		var items []Item
		if err := db.Where("order_uid = ?", order.OrderUID).Find(&items).Error; err != nil {
			return err
		}
		order.Items = items

		cache[order.OrderUID] = order
	}
	return nil
}
