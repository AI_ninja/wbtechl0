package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"gitlab.com/white-prince/wbtechl0/pkg/formatted"
	"gitlab.com/white-prince/wbtechl0/pkg/generate"
	"gorm.io/gorm"
	"math/rand"
	"sync"
	"testing"
	"time"
)

type MockDb struct {
	mock.Mock
}

func (m *MockDb) Create(value interface{}) *gorm.DB {
	args := m.Called(value)
	return args.Get(0).(*gorm.DB)
}

type TestCase struct {
	Name          string
	Order         models.Order
	IncorrectJson map[string]interface{}
}

func TestSubscribeHandler(t *testing.T) {
	dataAnalyze := generate.New()
	var testCases []TestCase
	for i := 0; i < 10; i++ {
		order := models.Order{
			OrderUID:          generate.RandomString(16),
			TrackNumber:       "WBILMTESTTRACK",
			Entry:             "WBIL",
			Delivery:          generate.RandomDelivery(dataAnalyze),
			Payment:           generate.RandomPayment(),
			Items:             []models.Item{generate.RandomItem()},
			Locale:            "en",
			InternalSignature: "",
			CustomerID:        rand.Intn(100),
			DeliveryService:   "meest",
			ShardKey:          "9",
			SmID:              rand.Intn(100),
			DateCreated:       time.Now().Format(time.RFC3339),
			OofShard:          "1",
		}

		order.Delivery.OrderUID = order.OrderUID
		order.Payment.OrderUID = order.OrderUID
		for idx := range order.Items {
			order.Items[idx].OrderUID = order.OrderUID
		}
		testCases = append(testCases, TestCase{Name: "Random data test", Order: order})
	}

	mockMutex := &sync.Mutex{}

	handler := &Handler{
		Cache: make(map[string]models.Order),
		Mu:    mockMutex,
		Ctx:   context.Background(),
	}

	for idx, tc := range testCases {
		mockDb := new(MockDb)
		mockDb.On("Create", mock.Anything).Return(&gorm.DB{})
		handler.Db = mockDb

		data, err := json.Marshal(tc.Order)
		assert.NoError(t, err)

		handler.messageHandler(data)

		mockDb.AssertNumberOfCalls(t, "Create", 3+len(tc.Order.Items))

		cachedOrder, exists := handler.Cache[tc.Order.OrderUID]
		assert.True(t, exists, "Order must be in cache")
		assert.Equal(t, tc.Order, cachedOrder, "Order in cache must be equal original order")
		assert.Equal(t, idx+1, len(handler.Cache), "Cache size must be equal to the number of processed orders")

		formatted.FormattedPrint(tc.Order)
		fmt.Println(len(handler.Cache))
	}
}

func TestHandlerSubscribeValidation(t *testing.T) {
	dataAnalyze := generate.New()
	testCases := make([]TestCase, 0, 10)
	for i := 0; i < 4; i++ {
		order := models.Order{
			OrderUID:          generate.RandomString(16),
			TrackNumber:       "WBILMTESTTRACK",
			Entry:             "WBIL",
			Delivery:          generate.RandomDelivery(dataAnalyze),
			Payment:           generate.RandomPayment(),
			Items:             []models.Item{generate.RandomItem()},
			Locale:            "en",
			InternalSignature: "",
			CustomerID:        rand.Intn(100),
			DeliveryService:   "meest",
			ShardKey:          "9",
			SmID:              rand.Intn(100),
			DateCreated:       time.Now().Format(time.RFC3339),
			OofShard:          "1",
		}

		testCase := TestCase{
			Order: order,
		}

		if i == 0 {
			testCase.Order.OrderUID = ""
			testCase.Name = "Empty OrderUID"
		} else if i == 1 {
			testCase.Order.Delivery.Name = ""
			testCase.Name = "Empty Delivery Name"
		} else if i == 2 {
			testCase.Order.Items[0].TrackNumber = ""
			testCase.Name = "Empty Track Number Name"
		} else if i == 3 {
			testCase.Order.Items = nil
			testCase.Name = "Items is empty"
		}
		testCases = append(testCases, testCase)
	}

	mockMutex := &sync.Mutex{}

	handler := &Handler{
		Cache: make(map[string]models.Order),
		Mu:    mockMutex,
		Ctx:   context.Background(),
	}

	for _, tc := range testCases {
		mockDb := new(MockDb)
		mockDb.On("Create", mock.Anything).Return(&gorm.DB{})
		handler.Db = mockDb

		data, err := json.Marshal(tc.Order)
		assert.NoError(t, err)

		handler.messageHandler(data)

		mockDb.AssertNumberOfCalls(t, "Create", 0)

		_, exists := handler.Cache[tc.Order.OrderUID]
		assert.False(t, exists, "Incorrect data not should be in cache")
	}
}

func TestSubscribeHandlerWithInvalidJson(t *testing.T) {
	mockDb := new(MockDb)
	handler := &Handler{
		Db:    mockDb,
		Cache: make(map[string]models.Order),
		Mu:    &sync.Mutex{},
		Ctx:   context.Background(),
	}

	invalidJson := "{invalid json"

	handler.messageHandler([]byte(invalidJson))

	assert.Equal(t, 0, len(handler.Cache), "Cache должен быть пустым после обработки некорректного JSON")

	mockDb.AssertNumberOfCalls(t, "Create", 0)
}
