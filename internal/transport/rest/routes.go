package rest

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/nats-io/stan.go"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"gitlab.com/white-prince/wbtechl0/internal/service"
	"log"
	"net/http"
)

func (h *Handler) mainPage(c *gin.Context) {
	h.Mu.Lock()
	c.HTML(http.StatusOK, "main.tmpl", gin.H{
		"Orders": h.Cache,
	})
	h.Mu.Unlock()
}

func (h *Handler) showPage(c *gin.Context) {
	id := c.Query("id")
	h.Mu.Lock()
	value := h.Cache[id]
	log.Println(value)
	c.HTML(http.StatusOK, "order.tmpl", gin.H{
		"Order": value,
	})
	h.Mu.Unlock()
}

func (h *Handler) SubscribeHandler(msg *stan.Msg) {
	h.messageHandler(msg.Data)
}

func (h *Handler) messageHandler(data []byte) {
	var order models.Order
	err := json.Unmarshal(data, &order)
	if err != nil {
		log.Printf("Error unmarshal formatted %v", err)
		return
	}

	if err = service.MessageValidation(&order); err != nil {
		log.Printf("Validation error: %v", err)
		return
	}

	result := h.Db.Create(&order)
	order.Delivery.OrderUID = order.OrderUID
	if result.Error != nil {
		log.Printf("Error create order %v", err)
		return
	}
	h.Db.Create(&order.Delivery)
	order.Payment.OrderUID = order.OrderUID
	h.Db.Create(&order.Payment)
	for idx := range order.Items {
		order.Items[idx].OrderUID = order.OrderUID
		h.Db.Create(&order.Items[idx])
	}

	service.WriteToCache(h.Mu, h.Cache, order.OrderUID, order)
}
