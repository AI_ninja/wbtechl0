package rest

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"net/http"
	"sync"
)

const (
	template = "web/templates/*"
	static   = "web/static/"
)

type Handler struct {
	Ctx   context.Context
	Mu    *sync.Mutex
	Cache map[string]models.Order
	Db    models.Database
}

func New(ctx context.Context, db models.Database) *Handler {
	return &Handler{
		Ctx:   ctx,
		Mu:    &sync.Mutex{},
		Cache: make(map[string]models.Order),
		Db:    db,
	}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.Default()

	router.LoadHTMLGlob(template)
	router.StaticFS("/static", http.Dir(static))

	router.GET("/favicon.ico", func(c *gin.Context) {
		c.Status(http.StatusNoContent)
	})

	router.GET("/", h.mainPage)
	router.GET("/page", h.showPage)

	return router
}
