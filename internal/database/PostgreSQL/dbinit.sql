CREATE TABLE Orders (
    order_uid VARCHAR PRIMARY KEY,
    track_number VARCHAR NOT NULL,
    entry VARCHAR NOT NULL,
    locale VARCHAR,
    internal_signature VARCHAR,
    customer_id INT NOT NULL,
    delivery_service VARCHAR,
    shardkey VARCHAR,
    sm_id INT,
    date_created TIMESTAMP,
    oof_shard VARCHAR
);

CREATE TABLE Deliveries (
    delivery_id SERIAL PRIMARY KEY,
    order_uid VARCHAR REFERENCES Orders(order_uid),
    name VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    zip VARCHAR,
    city VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    region VARCHAR,
    email VARCHAR NOT NULL
);

CREATE TABLE Payments (
    payment_id SERIAL PRIMARY KEY,
    order_uid VARCHAR REFERENCES Orders(order_uid),
    transaction VARCHAR NOT NULL,
    request_id VARCHAR,
    currency VARCHAR NOT NULL,
    provider VARCHAR NOT NULL,
    amount INT NOT NULL,
    payment_dt TIMESTAMP NOT NULL,
    bank VARCHAR,
    delivery_cost INT,
    goods_total INT NOT NULL,
    custom_fee INT
);

CREATE TABLE Items (
    item_id SERIAL PRIMARY KEY,
    order_uid VARCHAR REFERENCES Orders(order_uid),
    chrt_id INT NOT NULL,
    track_number VARCHAR NOT NULL,
    price INT NOT NULL,
    rid VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    sale INT,
    size VARCHAR,
    total_price INT NOT NULL,
    nm_id INT NOT NULL,
    brand VARCHAR NOT NULL,
    status INT NOT NULL
);

