truncate table items, payments, deliveries, orders;

alter sequence items_item_id_seq RESTART WITH 1;
alter sequence payments_payment_id_seq RESTART WITH 1;
alter sequence deliveries_delivery_id_seq RESTART WITH 1;