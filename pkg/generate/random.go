package generate

import (
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const (
	Name      = "Name"
	Cities    = "Cities"
	Addresses = "Addresses"
	Regions   = "Regions"
)

type RandomData struct {
	FirstNames []string
	LastNames  []string
	Cities     []string
	Addresses  []string
	Regions    []string
}

func New() *RandomData {
	return &RandomData{
		FirstNames: []string{"Test", "Alexander", "Boris", "Amir", "Timur", "Dmitriy", "Ekaterina"},
		LastNames:  []string{"Testov", "Nigmatzanov", "Vinogradov", "Birykov", "Putin", "Pushkin", "Demidov", "Dostoevskiy"},
		Cities:     []string{"Moscow", "Saint Petersburg", "Novosibirsk", "Yekaterinburg", "Nizhny Novgorod", "Kazan", "Chelyabinsk", "Omsk", "Samara", "Rostov-on-Don", "Ufa", "Krasnoyarsk", "Perm", "Voronezh", "Volgograd"},
		Addresses:  []string{"123 Red Square", "45 Nevsky Prospect", "89 Lenin Street", "56 Pushkin Avenue", "32 Gagarin Boulevard", "78 Tatarstan Street", "90 Revolution Street", "23 Siberian Lane", "67 Kuibyshev Street", "39 Bolshaya Sadovaya", "15 Kirov Street", "22 Mira Street", "19 Sibirskaya Street", "88 Plekhanovskaya Street", "77 Volga River Embankment"},
		Regions:    []string{"Moscow Region", "Leningrad Region", "Krasnodar Krai", "Sverdlovsk Region", "Rostov Region", "Chelyabinsk Region", "Samara Region", "Tatarstan Republic", "Bashkortostan Republic", "Krasnoyarsk Krai", "Perm Krai", "Nizhny Novgorod Region", "Stavropol Krai", "Khanty-Mansi Autonomous Okrug", "Volgograd Region"},
	}
}

func RandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}
func RandomDelivery(data *RandomData) models.Delivery {
	return models.Delivery{
		Name:    data.getData(Name),
		Phone:   randomNumber(),
		Zip:     strconv.Itoa(rand.Intn(1000000)),
		City:    data.getData(Cities),
		Address: data.getData(Addresses),
		Region:  data.getData(Regions),
		Email:   RandomString(5) + "@gmail.com",
	}
}

func RandomPayment() models.Payment {
	return models.Payment{
		Transaction:  RandomString(16),
		RequestID:    "",
		Currency:     "USD",
		Provider:     "wbpay",
		Amount:       rand.Intn(5000),
		PaymentDt:    time.Unix(1705669659, 0),
		Bank:         "alpha",
		DeliveryCost: rand.Intn(500),
		GoodsTotal:   rand.Intn(500),
		CustomFee:    0,
	}
}

func RandomItem() models.Item {
	return models.Item{
		ChrtID:      rand.Intn(100000),
		TrackNumber: "WBILMTESTTRACK",
		Price:       rand.Intn(500),
		Rid:         RandomString(16),
		Name:        "Item " + RandomString(5),
		Sale:        rand.Intn(100),
		Size:        "0",
		TotalPrice:  rand.Intn(500),
		NmID:        rand.Intn(100000),
		Brand:       "Brand " + RandomString(5),
		Status:      rand.Intn(100),
	}
}
func (r *RandomData) getData(kind string) string {
	switch {
	case kind == Name:
		var sb strings.Builder
		randomFirstName := r.FirstNames[rand.Intn(len(r.FirstNames))]
		randomLastName := r.LastNames[rand.Intn(len(r.LastNames))]

		sb.WriteString(randomFirstName)
		sb.WriteString(" ")
		sb.WriteString(randomLastName)

		return sb.String()
	case kind == Cities:
		return r.Cities[rand.Intn(len(r.Cities))]
	case kind == Addresses:
		return r.Addresses[rand.Intn(len(r.Addresses))]
	case kind == Regions:
		return r.Regions[rand.Intn(len(r.Regions))]
	}
	return ""
}

func randomNumber() string {
	var letters = []rune("1234567890")
	var sb strings.Builder
	sb.WriteString("+")

	s := make([]rune, 10)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	sb.WriteString(string(s))

	return sb.String()
}
