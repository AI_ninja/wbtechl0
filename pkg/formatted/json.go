package formatted

import (
	"encoding/json"
	"gitlab.com/white-prince/wbtechl0/internal/models"
	"log"
)

func FormattedPrint(order models.Order) {
	formattedJSON, err := json.MarshalIndent(order, "", "    ")
	if err != nil {
		log.Fatalf("Error MarshalIndent formatted JSON: %v", err)
	}
	log.Printf("Message send: %s", string(formattedJSON))
}
