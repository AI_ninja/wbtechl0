.PHONY: all sub pub testlog test valid

all:

sub:
	clear
	go run cmd/subscriber/main.go

pub:
	clear
	go run cmd/publisher/main.go

testlog:
	clear
	go test -v internal/transport/rest/*

test:
	clear
	go test internal/transport/rest/*

valid:
	clear
	go test -run TestHandlerSubscribeValidation internal/transport/rest/*

wrk:
	wrk -t6 -c200 -d5s 'http://localhost:8080/page?id=ISkkCwjdMxxcQmyY'
